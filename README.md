# Easy Circuit

Le but du projet est de réaliser une application permettant aux utilisateurs de consulter et de créer des circuits de visites personnalisé.

### Technologie
Le Frond est dévellopé avec ReactJs, sa base de donnée est géré avec Firebase ainsi que l'authentification

Le Back est dévellopé avec Django, sa base de donnée est géré avec Prosgres

La Solution est Dockerisé pour une instalation est un déploiment simplifié
### Commande shell
Pour lancer le client avec npm :
```
$ cd client/
$ npm install
```
Pour lancer le server depuis le terminal
```
$ envsubst < /etc/nginx/conf.d/nginx.tmpl > /etc/nginx/nginx.conf && exec nginx -g 'daemon off;'
```
Pour lancer la solution avec docker :
```
$ docker-compose build (client) // () si seulement le client
$ docker-compose up (client)
```
###  Client

L'Application est principalement composé de quatre pages.

* **/** La page public qui permet de consulter les circuits publiés.
* **/user** La page public qui permet de consulter les circuits publiés (en étant connecté).
* **/user/profile** Votre page utilisateur où l'on retrouve vos circuits, votre niveaux ainsi que d'autre donnée utilisateur.
* **/user/profile/editor/\*** La page d'édition de votre circuit.

###  Editor

<div align="center">
<img src="view/view1.png" alt="view app" width="360">
</div>

* **Search** Lorsque vous entrer un lieu, l'application vous présente ceux qu'il connait.
    Une fois cliquer le lieu est rajouter au parcourt.
* **Name** Ce champs permet d'écrire le nom de votre circuit.
* **Département** Ce champs permet de préciser le lieu du circuit, s'il reste vide il est visible dans toue les département.
* **Step Manual (in progress)** Permettra de posé son propre lieu s'il n'est pas trouvé par l'application.
* **Save** Permet de stocker dans son profile l'état actuelle du circuit.
* **Publish** Permet de publier le circuit dans son état actuelle.
