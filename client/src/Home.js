import React, { Component } from 'react';
import MapBox from './Map';
import RoadMap from './RoadMap';
//import { BrowserRouter as Router, Route, Link } from "react-router-dom";
class Home extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            openRoad: false,
            model:'login',
            places: []
          }
          this.OpenRoad = this.OpenRoad.bind(this);
          this.ChangePlaces = this.ChangePlaces.bind(this);
    }

    ChangePlaces(obj) {
        this.setState(state => ({
          places: obj
        }));
      }
      OpenRoad(road) {
        var open;
        if(road === true || road === false)
          open = road;
        else
          open = !this.state.openRoad;
        this.setState(state => ({
          openRoad: open
        }));
      }

    mode() {
      if (window.location.pathname !== "/user" && window.location.pathname !== "/"){
        return 'editor';
      }
      return 'public';
    }

    render() {
        return (
          <div>
            <MapBox details={this.mode()} changePlacesFunc={this.ChangePlaces} roadFunc={this.OpenRoad}/>
            <RoadMap openRoad={this.state.openRoad} roadFunc={this.OpenRoad} places={this.state.places} />
          </div>
        );
      }
}

export default Home;