import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';
import './SearchBox.css'
import cmarker from './assets/image/marker.png'
import DBFirebase from './firebaseConfig';
require ('firebase/auth');

const places = [
    {
        id: 0,
        name: 'The Achievement',
        adress: '11 Boulevard Jean Jaurès, 54000 Nancy',
        phone: '+330000000',
        avis: '80',
        place: 'Bar',
        picture: 'https://placekitten.com/g/340/',
        site: '',
        prix: '€'
    },
    {
        id: 1,
        name: 'La Taverne de L\'Irlandais',
        adress: '8 Rue Mazagran, 54000 Nancy',
        phone: '+330000000',
        avis: '75',
        place: 'Bar',
        picture: 'https://placekitten.com/g/294/',
        site: '',
        prix: '€'
    },
    {
        id: 2,
        name: 'Pub Mac Carthy',
        adress: '6 Rue Guerrier de Dumast, 54000 Nancy',
        phone: '+330000000',
        place: 'Bar',
        picture: 'https://placekitten.com/g/184/',
        site: 'google.fr',
        prix: '€€'
    },
    {
        id: 3,
        name: 'Le Bon Temps',
        adress: '13 Avenue Foch, 54000 Nancy',
        avis: '50',
        place: 'Bar',
        picture: 'https://placekitten.com/g/423/',
        site: 'google.fr',
        prix: '€€'

    },
    {
        id: 4,
        name: 'Les Docks',
        adress: '47 Rue Saint-Georges, 54000 Nancy',
        phone: '+330000000',
        avis: '90',
        place: 'Bar',
        picture: 'https://placekitten.com/g/422/',
        site: 'google.fr',
        prix: '€'
    },
];

let circuit = null;

var selectedCircuit = {id:-1, name:'', city:'', country:'', position:false};

  function escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }
  
  function getSuggestions(value) {
    const escapedValue = escapeRegexCharacters(value.trim());
    
    if (escapedValue === '') {
      return [];
    }
  
    const regex = new RegExp(escapedValue, 'i');
  
    return circuit
      .map(section => {
        return {
          country: section.country,
          circuit: section.circuit.filter(language => regex.test(language.name))
        };
      })
      .filter(section => section.circuit.length > 0).filter(section => section.country === selectedCircuit.country || selectedCircuit.country === "");
  }


  function getSuggestionValue(suggestion) {
    return suggestion.name;
  }
  
  function renderSuggestion(suggestion) {
    return (
      <span>{suggestion.name} - {suggestion.city}</span>
    );
  }
  
  function renderSectionTitle(section) {
    return (
      <strong>{section.country}</strong>
    );
  }
  
  function getSectionSuggestions(section) {
    return section.circuit;
  }

class SearchBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chevron: "<<",
            classButton: "close",
            value: '',
            suggestions: [],
            country: [],
            activeMarker:false,
            ready:false
        };
        this.getCountry = this.getCountry.bind(this);
        this.getCountry();
        DBFirebase.auth().onAuthStateChanged(user => {
            DBFirebase.database().ref('/public/circuit').once('value').then(snapshot => {
                let data = snapshot.val();
                circuit = data;
                this.setState(state => ({ ready:true }));
                circuit = Object.keys(circuit).map(function (key) {
                    return circuit[key];
                });
            });
        });
    }



    getCountry() {
        fetch('https://geo.api.gouv.fr/departements').then(response => {
            return response.json().then(json =>
                // traitement du JSON
                this.setState(state => ({
                    country: json.map(section => <option key={section.nom} value={section.nom}>{section.code+ '-' + section.nom}</option>)
                }))
            );
        });
    }

    moveSearchBox = event => {
        if (this.state.classButton === "close") {
            this.setState(state => ({
                classButton: "open", chevron: ">>"
            }));
        }
        else {
            this.setState(state => ({
                classButton: "close", chevron: "<<"
            }));
        }
    }

    onChange = (event, { newValue, method }) => {
        this.setState({
            value: newValue
        });
    };

    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: getSuggestions(value)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    onSuggestionSelected = (event, { suggestion, suggestionValue, suggestionIndex}) => {
        selectedCircuit.id = suggestion.id;
        console.log(this.props);
        this.props.roadFunc(true);
    }

    checkValue(value){
        return value != null ? value : '';
    }

    render() {
        const { value, suggestions } = this.state;
        const inputProps = {
            placeholder: "Trouver votre Circuit",
            value,
            onChange: this.onChange
        };
        return (
            <div id="volet" className={this.state.classButton}>
                <div id={!this.state.ready? 'hiden':'show'} className="search_input">
                    <Autosuggest
                        multiSection={true}
                        suggestions={suggestions}
                        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                        onSuggestionSelected={this.onSuggestionSelected}
                        getSuggestionValue={getSuggestionValue}
                        renderSuggestion={renderSuggestion}
                        renderSectionTitle={renderSectionTitle}
                        getSectionSuggestions={getSectionSuggestions}
                        inputProps={inputProps} />
                </div>
                <div id={this.props.details == "editor" ? "show" : "hiden"} className="search_input">
                <input className="circuitNameInput" name="name"  onChange={this.props.handleChange} placeholder="Circuit name"/>
                </div>
                <select className="form-control form-control-lg land-select"name="country" onChange={(e) =>  this.props.details == "public"?selectedCircuit.country = e.target.value:this.props.handleChange(e)}>
                    {this.props.details == "public" ? <option value="">All</option> : <option value="">Departement</option>}
                    {this.state.country}
                </select>
                <div id={this.props.details == "public" ? "show" : "hiden"} className="inputGroup">
                    <input type="checkbox" className="switch_1" onClick={() => selectedCircuit.position = !selectedCircuit.position} />
                    <label className="text-position">Add my position in route</label>
                </div>
                <div id={this.props.details == "editor" ? "show" : "hiden"} className="inputGroup">
                    <button className={this.state.activeMarker == true ? "on":"off"} onClick={() => this.setState(state => ({ activeMarker:this.props.mapmanual()}))}><img src={cmarker}/></button>
                    <label className="text-step">Add my step manually</label>
                </div>
                <div id={this.props.details == "editor" ? "show" : "hiden"} className="inputGroup last">
                    <button onClick={()=>this.props.handleChange('publish')} ><i className="fas fa-globe-europe"></i></button>
                </div>
                <div id={this.props.details == "editor" ? "show" : "hiden"} className="inputGroup last">
                    <button onClick={()=>this.props.handleChange('save')} ><i className="fas fa-save"></i></button>
                </div>
                <button onClick={this.moveSearchBox} className="chevron">{this.state.chevron}</button>
            </div>
        );
    }
}
            
export {SearchBox, selectedCircuit, circuit, places};