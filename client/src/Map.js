import React, { Component } from 'react';
import mapboxgl from 'mapbox-gl'
import 'mapbox-gl/dist/mapbox-gl.css';
import MapboxDirections from '@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions'
import MapboxGeocoder from 'mapbox-gl-geocoder'
import './Map.css';
import {SearchBox,selectedCircuit, circuit, places } from './SearchBox';
import cmarker from './assets/image/marker.png'
import amarker from './assets/image/atarget.png'
import dmarker from './assets/image/dtarget.png'
import DBFirebase from './firebaseConfig';
import {setValue} from './utils';
require ('firebase/auth');

var map = null;
var directions = null;
var geoPositionControl;
var geoCoderControl;
var position = { coords: [0,0], active: false };

function addWayPointMarker(data, index, type) {
    var el = document.createElement('div');
    var img = document.createElement('img');
    var txt = document.createElement('label');
    el.appendChild(img);
    el.appendChild(txt);
    if (type === 1 && index != 0){
        txt.innerHTML = index;
        img.src = cmarker;
        el.style.marginTop = '-25px';

    }
    else if (type === 1){
        img.src = cmarker;
        el.style.marginTop = '-25px';
    }
    else if (type === 2)
        img.src = amarker;
    else
        img.src = dmarker;
    txt.style.textAlign = "center";
    txt.style.fontSize = '30px';
    txt.style.color = '#34495e';
    txt.style.marginLeft = '-34px';
    txt.style.verticalAlign = 'sub';
    img.style.width = '50px';
    img.style.height = '50px';
    setTimeout(function () { directions.on('route', function () { el.remove() }); }, 1000);
    new mapboxgl.Marker(el)
        .setLngLat(data)
        .addTo(map)
}

function watch(obj, prop, handler) { // make this a framework/global function
    var currval = obj[prop];
    function callback() {
        if (obj[prop] !== currval) {
            var temp = currval;
            currval = obj[prop];
            handler(temp, currval);
        }
    }
    return callback;
}
class MapBox extends Component {

    constructor(props) {
        super(props);
        let circuitid = window.location.pathname.substr(window.location.pathname.lastIndexOf('/') + 1);
        this.state = {
            active:false,
            circuitid: circuitid == 'editor'? undefined : circuitid,
            userid: '',
            circuit: {
                name:'',
                country:''
            },
        };
        if (this.state.circuitid != undefined && this.props.details === "editor") {
            DBFirebase.auth().onAuthStateChanged(user => {
                this.setState(state => ({ userid: DBFirebase.auth().currentUser.uid }));
                DBFirebase.database().ref('/users/' + DBFirebase.auth().currentUser.uid
                    + '/circuit/' + this.state.circuitid).once('value').then(snapshot => {
                        let data = snapshot.val();
                        this.setState(state => ({ circuit: data}));
                        if (this.state.circuit.route)
                            setTimeout(() =>this.myCircuit(null,this.state.circuit), 2000);
                    });
            });
        }
        this.myCircuit = this.myCircuit.bind(this);
        this.myPosition = this.myPosition.bind(this);
        this.mapManual = this.mapManual.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.maping();
        if (this.props.details === "editor")
            this.mapPrivate();
        setInterval(watch(selectedCircuit, "id", this.myCircuit), 100);
        setInterval(watch(selectedCircuit, "position", this.myPosition), 100);
    }
    
    myPosition(oldval, newval) {
        position.active = newval;
        geoPositionControl.trigger();
        setTimeout(() => {
            if (selectedCircuit.id !== -1)
                this.myCircuit(0, selectedCircuit.id);},200);
        
    }

    myCircuit(oldval, newval) {
        var target = null;
        var waypointIndex = 0;
        directions.removeRoutes();
        if (this.props.details !== "editor")
            circuit.forEach(element => {
                element.circuit.forEach(item => {
                    if (item.id === newval)
                        target = item.route.slice(0);
                });
            });
        else
            target = newval.route.slice(0);
        var newplaces = [];
            var lastCoords = null;
            if (position.active) {
                directions.setOrigin([position.coords.longitude, position.coords.latitude]);
                lastCoords = [position.coords.longitude, position.coords.latitude];
                addWayPointMarker(lastCoords, waypointIndex + 1,0);
            }
            else {
                directions.setOrigin(target[0].data);
                lastCoords = target[0].data;
                newplaces.push(target[0].place);
                addWayPointMarker(target[0].data, waypointIndex + 1,0);
                target.shift();
            }
            while (0 != target.length) {
                if (waypointIndex !== 0 || position.active) {
                    target.sort(function (a, b) {
                        var somb = Math.sqrt(Math.pow(b.data[0] - lastCoords[0], 2) + Math.pow(b.data[1] - lastCoords[1], 2));
                        var soma = Math.sqrt(Math.pow(a.data[0] - lastCoords[0], 2) + Math.pow(a.data[1] - lastCoords[1], 2));
                        var coord = soma - somb;
                        return coord;
                    });
                    newplaces.push(target[0].place);
                    lastCoords = target[0].data;
                    if (1 === target.length){
                        directions.setDestination(target[0].data);
                        addWayPointMarker(target[0].data, waypointIndex + 1, 2);
                    }
                    else {
                        directions.addWaypoint(waypointIndex, target[0].data);
                        addWayPointMarker(target[0].data, waypointIndex + 1,1);
                    }
                    target.shift();
                }
                waypointIndex++;
            }
        console.log(newplaces);
        this.props.changePlacesFunc(newplaces);
    };

    mapManual() {
            this.setState(state => ({
                active: !this.state.active
            }));
            console.log(this.state.active);
            return !this.state.active;
    }

    mapPrivate() {
        map.on('click', e => {
            if (this.state.active) {
                var el = document.createElement('div');
                var img = document.createElement('img');
                var txt = document.createElement('label');
                el.appendChild(img);
                el.appendChild(txt);
                el.className = 'marker';
                img.src = cmarker;
                txt.style.textAlign = "center";
                txt.style.fontSize = '30px';
                txt.style.color = '#34495e';
                txt.style.marginLeft = '-34px';
                txt.style.verticalAlign = 'sub';
                img.style.width = '50px';
                img.style.height = '50px';
                var marker = new mapboxgl.Marker(el)
                    .setLngLat(e.lngLat)
                    .addTo(map)
                    .setDraggable(true)
                el.addEventListener('contextmenu', () => {
                    el.remove();
                });
            }
        });
        geoCoderControl = new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
        });
        map.addControl(geoCoderControl, 'top-right');
        map.on('load', function (e) {
            document.getElementsByClassName('search_input')[0].firstChild.remove();
            document.getElementsByClassName('search_input')[0].appendChild(document.getElementsByClassName('mapboxgl-ctrl-top-right')[0]);
            });
        geoCoderControl.on('result', ev=> {
            console.log(ev.result);
            let tmpcircuit = Object.assign({}, this.state.circuit);
            if (tmpcircuit.route == undefined){
                tmpcircuit.route = [];
                tmpcircuit.city = ev.result.context[1].text;
            }
            tmpcircuit.route.push({
                data:ev.result.center,
                place:{
                    name:ev.result.text,
                    adress:ev.result.properties.address+", "+ ev.result.context[1].text,
                    place:ev.result.properties.category
                }
            });
            this.setState({circuit: tmpcircuit});
            this.myCircuit(null,this.state.circuit);
        });
        }

    maping() {

        mapboxgl.accessToken = 'pk.eyJ1IjoibHVjYXNkZXByZXQiLCJhIjoiY2pweTE0ZzM5MDRjZTQzbXJkenA0cGo2YyJ9.74KShhWBhzsSgad1GA-ZpA';
        map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v9',
            center: [6.19, 48.693], // starting position [lng, lat]
            pitch: 20,
            bearing: -15,
            zoom: 4 // starting zoom
        });
        directions = new MapboxDirections({
            accessToken: mapboxgl.accessToken,
            interactive: false,
            unit: 'metric',
            profile: 'mapbox/walking',
        });
        map.addControl(directions, 'top-left');
        geoPositionControl = new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true
            },
            trackUserLocation: true
        });
        map.addControl(geoPositionControl, "bottom-right");
        map.addControl(new mapboxgl.NavigationControl(), "bottom-left");
        document.getElementsByClassName('mapboxgl-ctrl-top-left')[0].style.display = 'none';
        map.on('load', function () {
            geoPositionControl.on("geolocate", function (e) { position.coords = e.coords });
        });
    }
    handleChange(event) {
        if (event == 'publish') {
            try {
                DBFirebase.database().ref('/users/' + DBFirebase.auth().currentUser.uid).once('value').then(snapshot => {
                    let data = snapshot.val();
                    var updates = {};
                    updates['users/' + this.state.userid + '/userXp'] = data.userXp + 20;
                    DBFirebase.database().ref().update(updates);
                    return DBFirebase.database().ref('public/circuit/' + this.state.circuitid).set({
                        country: this.state.circuit.country,
                        circuit: [this.state.circuit]
                    });
                });
                return;
            } catch (e) {
                var updates = {};
                updates['public/circuit/' + this.state.circuitid] = this.state.circuit;
                return DBFirebase.database().ref().update(updates);
            }
        }
        if (event == 'save') {
            var updates = {};
            updates['users/' + this.state.userid + '/circuit/' + this.state.circuitid] = this.state.circuit;
            return DBFirebase.database().ref().update(updates);
        }
        console.log(event.target);
        var i = 0;
        let tmpcircuit = Object.assign({}, this.state.circuit);
        setValue(event.target.name, event.target.value, tmpcircuit);
        console.log(tmpcircuit);
        this.setState({circuit: tmpcircuit});
    }
    render() {
        return (
            <div>
                <div id="map"></div>
                <SearchBox details={this.props.details} roadFunc={this.props.roadFunc}
                    mapmanual={this.mapManual} handleChange={this.handleChange}/>
            </div>
        );
    }
}

export default MapBox;