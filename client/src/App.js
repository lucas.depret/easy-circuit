import React, { Component } from 'react';
import './App.css';
import NavBar from './NavBar';
import Auth from './Auth';
import Home from './Home';
import Profile from './Profile';
import NavBarUser from './NavBarUser';
import { BrowserRouter as Router, Route } from "react-router-dom";
import DBFirebase from './firebaseConfig';
import {circuit} from './SearchBox';
require ('firebase/auth');

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      openModal: false,
      model:'login',
    }
    this.OpenModal = this.OpenModal.bind(this);
    DBFirebase.auth().onAuthStateChanged(function (user) {
      if (DBFirebase.auth().currentUser && window.location.pathname == '/')
        window.location = window.location + 'user';
      else if (!DBFirebase.auth().currentUser && window.location.pathname != '/')
        window.location = window.location.origin;
    })
  }


OpenModal(model) {
  this.setState(state => ({
    openModal: !state.openModal,
    model: model
  }));
}

  componentDidMount() {
  }

  render() {
    return (
      <Router>
      <div className="App">
        <header className="App-header">
          <Auth openModal={this.state.openModal} modalFunc={this.OpenModal} model={this.state.model}/>
          <NavBar modalFunc={this.OpenModal}/>
        </header>
        <Route path="/user"  component={NavBarUser} />
        <Route exact path="/"  component={Home} />
        <Route exact path="/user"  component={Home} />
        <Route path="/user/profile/editor" component={Home} />
        <Route exact path="/user/profile"  component={Profile} />
      </div>
      </Router>
    );
  }
}

export default App;
