import React, { Component } from 'react';
import './App.css'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class NavBarUser extends Component {

    ongle(path) {
        if (window.location.pathname == path)
            return 'active';
        return '';
    }

    render() {
        return (
            <div className="nUser">
                <ul>
                    <li className={this.ongle("/user")}><Link to="/user">Home</Link></li>
                    <li className={this.ongle("/user/profile")}><Link to="/user/profile">Profile</Link></li>
                    <li className={this.ongle("/user/contact")}><Link to="/user/contact">Contact</Link></li>
                    <li className={this.ongle("/user/about")}><Link to="/user/about">About</Link></li>
                </ul>
            </div>
        );
    }
}

export default NavBarUser
