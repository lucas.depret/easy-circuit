import React, { Component } from 'react'
import './Profile.css'
import Circuit from './Circuit'
import DBFirebase from './firebaseConfig';
require ('firebase/auth');

class Profile extends Component {

    constructor(props) {
        super(props);
        this.state = {
          photo: '',
          name:'',
          type: '',
          level: 1,
          xp: 0,
          max:0,
          min:0
        }
        DBFirebase.auth().onAuthStateChanged(user => {
        DBFirebase.database().ref('/users/' + DBFirebase.auth().currentUser.uid).once('value').then(snapshot => {
            let data = snapshot.val();
            this.setState(state => ({
                photo:data.picture,
                name:data.username,
                xp:data.userXp
            }));
            DBFirebase.database().ref('/public/level').once('value').then(snapshot => {
                let data = snapshot.val();
                var level = data.filter(level => (level.min <= this.state.xp && this.state.xp < level.max));
                this.setState(state => ({
                    min:level[0].min,
                    max:level[0].max,
                    level:level[0].level,
                    type:level[0].grade
                }));
              });
          });
        });
    }

    render(){
        const calc = (this.state.xp - this.state.min) / (this.state.max - this.state.min);
        const progress = {
            width: [''+calc*100 + '%']
        };

        return (
            <div className="profile">
                <div className="profileUser">
                    <div className="profileUserUp">
                        <img src={this.state.photo} />
                        <h4>{this.state.name}</h4>
                        <span>{this.state.type + '・ Level ' + this.state.level}</span>
                        <div className="progressRow">
                            <div>{this.state.min}</div>
                            <div className="progress">
                                <div className="progress-bar" style={progress} role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">{calc*100 + '%'}</div>
                            </div>
                            <div>{this.state.max}</div>
                        </div>
                    </div>
                    <div className="profileUserDown">
                        <ul className="nav nav-tabs">
                            <li className="nav-item nav-link active">Badge</li>
                            <li className="nav-item nav-link">News</li>
                            <li className="nav-item nav-link">Avis</li>
                        </ul>
                    </div>
                </div>
                <Circuit/>
            </div>
        );
    }
}

export default Profile