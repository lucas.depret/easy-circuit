import React, { Component } from 'react';
import './App.css'
import logo from './assets/image/easyCircuit-light.png'
import { BrowserRouter as Router, Route } from "react-router-dom";
import DBFirebase from './firebaseConfig';
require ('firebase/auth');
class NavBar extends Component {

    constructor(props){
        super(props)
        this.Login = this.Login.bind(this);
    }
    componentDidMount() {
    }

    Login () {
        return (
            <div className="btn-group" role="group" aria-label="Basic example">
                    <button type="button" className="btn btn-secondary btn-navbar" onClick={() => this.props.modalFunc('Login')}>Login</button>
                    <button type="button" className="btn btn-secondary btn-navbar" onClick={() => this.props.modalFunc('Sign-in')}>Sign-in</button>
            </div>
        );
    }

    Deconnect () {
        var signout = function() {DBFirebase.auth().signOut().then(function() {
            window.location = window.location.origin;
          }).catch(function(error) {
            // An error happened.
          });
        }
          return (
            <div className="btn-group" role="group" aria-label="Basic example">
                    <button type="button" className="btn btn-secondary btn-navbar" onClick={() => signout()}>Disconnect</button>
            </div>
        );
    }

    render() {
        return (
            <Router>
            <div className="navbar">
                <img className="logo" src={logo} alt={logo}/>
                <h1 className="title">EasyCircuit</h1>
                <Route exact path="/" component={this.Login}></Route>
                <Route path="/:other" component={this.Deconnect}></Route>
            </div>
            </Router>
        );
    }
  }

  export default NavBar