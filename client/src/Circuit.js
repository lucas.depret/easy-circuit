import React, { Component } from 'react'
import { places } from './SearchBox';
import {Link} from "react-router-dom";
import {idGen} from './utils';
import DBFirebase from './firebaseConfig';
require ('firebase/auth');

const cir = [
    {
        id: 0,
        name: "Tournée des bars",
        modif: '2019-03-03',
        share: 'Myself'
    },
    {
        id: 1,
        name: "Lieu insolite",
        modif: '2018-05-03',
        share: 'Everything'
    }
]

class Circuit extends Component {
    constructor(props){
        super(props);
        this.state = {
            circuit: [],
            userid: ''
        }
        this.createCircuit = this.createCircuit.bind(this);
        DBFirebase.auth().onAuthStateChanged(user => {
            this.setState(state => ({ userid: DBFirebase.auth().currentUser.uid }));
            DBFirebase.database().ref('/users/' + DBFirebase.auth().currentUser.uid + '/circuit').once('value').then(snapshot => {
                let data = snapshot.val();
                let tmpcircuit =[];
                for (var key in data) {
                    if (data.hasOwnProperty(key)) {
                        console.log(data[key].id);
                        tmpcircuit.push({
                            id:data[key].id,
                            modif: data[key].modifiedAt,
                            share:data[key].share,
                            name: (data[key].name ? data[key].name : 'untitled')
                        });
                    }
                }
                this.setState(state => ({ circuit: tmpcircuit }));
                console.log(data);
            });
        });
    }
    openCircuit(key) {
        window.location = window.location.origin + "/user/profile/editor/" + key;
    }
    createCircuit() {
        const id = idGen();
        DBFirebase.database().ref('users/' + this.state.userid + '/circuit/' + id).set({
            id: id,
            modifiedAt: new Date().toLocaleDateString(),
            share: 'Myself',
            country: 'All'
        });
        this.openCircuit(id);
    }
    render() {
        const circuit = this.state.circuit.map(element =>
            <div key={element.id} className="List" onClick={()=>this.openCircuit(element.id)}>
            <div className="menuother"><i className="fas fa-map"></i></div>
            <div className="menuTitle">{element.name}</div>
            <div className="menuModified">{element.modif}</div>
            <div className="menuShare">{element.share}</div>
            <div className="menuotherlast"><i className="fas fa-ellipsis-v"></i></div>
            </div>);
        return (
            <div className="profileCircuit">
                <h3 className="circuitTitle">Circuit</h3>
                <div className="containerAction">
                <Link className="actionNew" to="/user/profile/editor" onClick={this.createCircuit}>New Circuit</Link>
                </div>
                <div className="circuitContainer">
                    <div className="containerMenu">
                    <div className="menuother"></div>
                        <div className="menuTitle">Name</div>
                        <div className="menuModified">Modified</div>
                        <div className="menuShare">Share</div>
                        <div className="menuotherlast"></div>
                    </div>
                    <div className="containerList">
                    {circuit}
                    </div>
                </div>
            </div>
        );
    }
}

export default Circuit;