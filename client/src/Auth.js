import React, { Component } from 'react';
import './Auth.css';
import DBFirebase from './firebaseConfig';
require ('firebase/auth');


class Auth extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username:'',
            mail: '',
            password: '',
            passwordagain: '',
        }
        this.handleChange = this.handleChange.bind(this);
        this.successRegister = this.successRegister.bind(this);
    }

    successRegister(mail, password, username) {
        var user = DBFirebase.auth().currentUser;
        var userID = user.uid;
        console.log(userID);
        DBFirebase.database().ref('users/' + userID).set({
            username: username,
            userEmail: mail,
            userPassword: password,
            userXp:0,
            userCreated: new Date().toLocaleDateString(),
            picture: 'https://www.qualiscare.com/wp-content/uploads/2017/08/default-user.png'
        });
        DBFirebase.database().ref('/users').once('value').then(function (snapshot) {
            console.log(snapshot.val());
            // ...
          });
        this.props.modalFunc('Sign-in');
        this.connect();
    }

    register() {
        console.log(this.state);
        var er = false;
        DBFirebase.auth().createUserWithEmailAndPassword(this.state.mail, this.state.password)
        .then((user) => this.successRegister(this.state.mail, this.state.password, this.state.username)).catch(function (error) {
            // Handle Errors here.
            er = true;
            console.log(error);
            var errorCode = error.code;
            var errorMessage = error.message;
            if (errorCode == 'auth/weak-password') {
                alert('The password is too weak.');
            } else {
                alert(errorMessage);
            }
            // ...
        });
        
    }

    connect() {
        DBFirebase.auth().signInWithEmailAndPassword(this.state.mail, this.state.password)
        .then((user) => window.location = window.location + 'user').catch(function(error) {
            // Handle Errors here.
            console.log(DBFirebase.auth().currentUser);
            var errorCode = error.code;
            alert(error.message);
            // ...
          });
    }

    choose(model) {
        if (model == 'Sign-in') this.register(); else this.connect();
    }

    handleChange(event) {
        console.log(event.target.name);
        this.setState({[event.target.name]: event.target.value});
      }

    signIn() {
        return(
            <div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon2"><i className="fas fa-user"></i></span>
                    </div>
                    <input type="text" className="form-control" name='username' onChange={this.handleChange} placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon2"><i className="fas fa-at"></i></span>
                    </div>
                    <input type="text" className="form-control" name='mail' onChange={this.handleChange} placeholder="Mail" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon2"><i className="fas fa-key"></i></span>
                    </div>
                    <input type="password" className="form-control" name="password" onChange={this.handleChange} placeholder="Password" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon2"><i className="fas fa-key"></i></span>
                    </div>
                    <input type="password" className="form-control" name="passwordagain" onChange={this.handleChange} placeholder="Password" aria-label="Username" aria-describedby="basic-addon1" />
                </div>
            </div>
        );
    }

    logIn() {
        return(
            <div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1"><i className="fas fa-at"></i></span>
                    </div>
                    <input type="text" className="form-control" name="mail" onChange={this.handleChange} placeholder="Mail" aria-label="Mail" aria-describedby="basic-addon1" />
                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1"><i className="fas fa-key"></i></span>
                    </div>
                    <input type="password" className="form-control" name="password" onChange={this.handleChange} placeholder="Password" aria-label="Password" aria-describedby="basic-addon1" />
                </div>
            </div>
        );
    }

    componentDidMount() {
    }

    render() {
        const model = this.props.model;
        let body;
    if (model == 'Sign-in') {
      body = this.signIn();
    } else {
      body = this.logIn();
    }
        return (
            <div className={this.props.openModal ? "show": "hide"}>
            <div className="modal" role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                                <h5 className="modal-title">{model}</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={() => this.props.modalFunc('Sign-in')}>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                {body}
                            </div>
                            <div className="modal-footer">
                                <button type="button" onClick={() => this.choose(model)} className="btn btn-primary">Let's Go</button>
                            </div>
                        </div>
                    </div>
            </div>
            </div>
        );
    }
}

export default Auth;