import React, {Component, PropTypes} from 'react';
import { Accordeon, Panel, Nav, Content } from 'react-accordeon';
import {places} from './SearchBox';

import './RoadMap.css'

const Footer = (props) => {
    const { expandAll, collapseAll } = props;
    const style = {
        bottom: '0',
        marginLeft: '10px',
        color:'#ecf0f1',
        cursor: 'pointer'
    }
    return (
        <div style={style}>
            <span onClick={expandAll}>Expand All</span> / <span onClick={collapseAll}>Collapse All</span>
        </div>
    );
};

  const Header = (props) => {
    return (
      <div>
      </div>
    );
  };
  Footer.propTypes = {
    expandAll: React.PropTypes.func,
    collapseAll: React.PropTypes.func,
  };

  const Navig = (props) => {
    const { expanded, toggle, title } = props;
    return (
      <div>
        <button className="step" onClick={toggle} aria-expanded={expanded} role="tab">{title}</button>
      </div>
    );
  };

    Navig.propTypes = {
    toggle: PropTypes.func,
    expanded: PropTypes.bool,
  };

class RoadMap extends Component {

    // constructor(props) {
    //     super(props);
    // }
    componentDidMount() {
    }

    avis(value) {
        if (value > 65)
            return "fas fa-smile"
        else if (value > 45)
            return "fas fa-meh"
        else
            return "fas fa-frown"
    }

    render() {
        var i = 1;
        const panels = this.props.places.map(places =>
        <Panel key={i}>
        <Nav><Navig title={i++ +" - "+ places.name}/></Nav>
                <Content>
                    <div className="roadmap-container">
                        <div className="roadmap-container-up">
                            <img src={places.picture ? places.picture : 'https://placekitten.com/g/422/'} />
                            <div className="roadmap-container-info">
                                <h2>Lieu :{places.place}</h2>
                                <div className="avis-prix"><i className={this.avis(places.avis?places.avis:55)}> {places.avis?places.avis:55}%</i>・{places.prix?places.prix:'Free'}</div>
                                <p><a href={places.site}>{places.site}</a></p>
                            </div>
                        </div>
                        <div className="roadmap-container-down">
                            <h5>{places.adress}{places.phone ? '・' : ''}{places.phone}</h5>
                        </div>
                    </div>
                </Content>
            </Panel>);
        return (
            <div className={this.props.openRoad ? "showRoad" : "hideRoad"}>
                <div className="roadmap">
            <button className="roadmap-volet" onClick={this.props.roadFunc}><i className="fas fa-angle-up"></i></button>
                <div className="roadmap-content">
                    <Accordeon header={<Header />} footer={<Footer />}>
                        {panels}
                    </Accordeon>
                </div>
                </div>
            </div>
        );
    }
  }

  export default RoadMap