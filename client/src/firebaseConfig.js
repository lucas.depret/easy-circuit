import * as firebase from 'firebase';

var DBFirebase = firebase.initializeApp({
    apiKey: "AIzaSyAakPAM22k5hCA55qEeyUgEC69XmbF4m9E",
    authDomain: "easycircuit-60971.firebaseapp.com",
    databaseURL: "https://easycircuit-60971.firebaseio.com",
    projectId: "easycircuit-60971",
    storageBucket: "easycircuit-60971.appspot.com",
    messagingSenderId: "1037166467279"
});

export default DBFirebase;